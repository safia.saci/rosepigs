#!/bin/bash
#SBATCH -J ROSEpigs_medpseq_chipseq
#SBATCH -p unlimitq
#SBATCH --mem=6G
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH -o output.out
#SBATCH -e error.out

module purge
module load bioinfo/nfcore-Nextflow-v20.11.0-edge

nextflow run nf-core/chipseq -r 1.2.1 -profile genotoul --seq_center genotoul -resume \
        --input rosePigs_medPseq_file_.csv \
        --fasta Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
        --gtf Sus_scrofa.Sscrofa11.1.102.gtf \
        --save_reference \
        --macs_gsize 2.25e9 \
        --min_reps_consensus 3
