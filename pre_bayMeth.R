# Packeges --------
library("dplyr")
library("Repitools")
library("stringr")
library("purrr")
library(rtracklayer)
library("BSgenome.Sscrofa.ENSEMBL.susScr11")

#windows genome
windows <- genomeBlocks(Sscrofa, width=100, spacing=100)
# CPG density files
cpgDens <- cpgDensityCalc(windows, organism=Sscrofa, w.function="linear", window=500)


# saveRDS(windows, file = "/work2/genphyse/genepi/guillaume/rosepigs/windows_full_genome.RDS")
# save(cpgDens, file = "/work2/genphyse/genepi/guillaume/rosepigs/cpgDens.RData")

# windows_genome <- readRDS("/work2/genphyse/genepi/guillaume/rosepigs/windows_full_genome.RDS")
# load("/work2/genphyse/genepi/guillaume/rosepigs/cpgDens.RData")

cpgdens <- windows_genome
cpgdens$score <- cpgDens
seqlengths(cpgdens) <- seqlengths(Sscrofa)

export(cpgdens, "/work2/genphyse/genepi/guillaume/rosepigs/cpgDens.Sscrofa.11.ensembl.bigWig", format = "bigWig")

methyl1 <- readRDS("/work2/genphyse/genepi/guillaume/rosepigs/annotationBlockCountRDS/Tem-methyl_R1.rds")
methyl2 <- readRDS("/work2/genphyse/genepi/guillaume/rosepigs/annotationBlockCountRDS/Tem-methyl_R2.rds")
methyl3 <- readRDS("/work2/genphyse/genepi/guillaume/rosepigs/annotationBlockCountRDS/Tem-methyl_R3.rds")
methyl4 <- readRDS("/work2/genphyse/genepi/guillaume/rosepigs/annotationBlockCountRDS/Tem-methyl_R4.rds")


control_minus <- matrix(c(methyl1, methyl2), ncol  = 2)
control_plus <- matrix(c(methyl3, methyl4), ncol  = 2)

controls_p <- matrix(rowSums(control_plus))
controls_m <- matrix(rowSums(control_minus))

saveRDS(controls_p, file = "/work2/genphyse/genepi/guillaume/rosepigs/control_plus.rds")
saveRDS(controls_m, file = "/work2/genphyse/genepi/guillaume/rosepigs/control_minus.rds")


