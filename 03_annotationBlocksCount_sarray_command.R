# Genrate a file with one command per line for an array job
library(dplyr)
library(purrr)
library(stringr)

prefix <- "/work2/genphyse/genepi/data/porc/medpseq/ROSEpigs_processed2/results/bwa/mergedLibrary/"
bam_data <- tibble(
    path = list.files(prefix, pattern = "*.bam$"),
    id = map_chr(strsplit(path, ".", fixed = TRUE), 1),
    type = str_sub(id, end = -4)
)

commands <- paste0(
    "./annotationBlocksCount.R -i ",
    prefix,
    bam_data$path,
    " -w /work2/genphyse/genepi/guillaume/rosepigs/windows_full_genome.RDS -o ",
    bam_data$id, ".rds"
)

write.table(
    commands,
    file = "annotationBlocksCount_sarray.sh",
    quote = F, row.names = F, col.names = F
)

# module load compiler/gcc-7.2.0
# module load module load system/R-4.0.2_gcc-7.2.0
# sarray -J abc -o %j.out -e %j.err -t 01:00:00 --mem=32G --mail-type=FAIL annotationBlocksCount_sarray.sh
