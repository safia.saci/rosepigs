library(GreyListChIP)
library(Repitools)
library("BSgenome.Sscrofa.ENSEMBL.susScr11")
library(purrr)
library(tibble)


windows <- genomeBlocks(Sscrofa, width = 500, spacing = 500)
greyList <- new("GreyList", tiles = windows, genome = Sscrofa)

prefix <- "/work2/genphyse/genepi/data/porc/medpseq/ROSEpigs_processed2/results/bwa/mergedLibrary"
bam_files <- list.files(prefix, pattern = "Tem-input.*.bam$", full.names = TRUE)

greyList <- lapply(bam_files, function(x) countReads(greyList, x))

greyList <- lapply(greyList, function(x) calcThreshold(x, reps = 10, sampleSize = 1000, p = 0.99, cores = 1))

greyList <- lapply(greyList, function(x) makeGreyList(x, maxGap=10000))

lapply(1:4, function(x) export(greyList[[x]], con = paste0("greyList_", x, ".bed")))


gl <- union(greyList[[1]]@regions,
      union(greyList[[2]]@regions,
            union(greyList[[3]]@regions,
                  greyList[[4]]@regions)))

export(gl, con = "gl.bed", format = "BED")



















# ou alors !!! ---------------

for (bam in bam_files) {
    greyList <- countReads(greyList, bam)
    greyList <- calcThreshold(greyList, reps = 10, sampleSize = 1000, p = 0.99, cores = 1)
    greyList <- makeGreyList(greyList, maxGap=10000)
    con = paste0(map_chr(strsplit(map_chr(strsplit(bam, "/", fixed = TRUE), 12), ".", fixed = TRUE), 1), "_grey_list.bed")
    export(object = greyList, con = con, format = "BED")
}




