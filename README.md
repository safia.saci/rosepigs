# Processing 
## RnaSeq 
Processing of rnaseq data using [nf-core/rnaseq](https://github.com/nf-core/rnaseq) V3.0 on genotoul using:  

[rnaseq_nfcore_test3.sh](https://forgemia.inra.fr/safia.saci/rosepigs/-/blob/master/rnaseq_nfcore_test3.sh)  

Results at : 
>  /work2/genphyse/genepi/data/porc/rnaseq/ROSEpigs_processed2/results
## MeDPSeq
Processing of MeDPSeq data using [nf-core/chipseq](https://github.com/nf-core/chipseq) V1.2.1 on genotoul using:  

[medpseq_nfcore_chipseq.sh](https://forgemia.inra.fr/safia.saci/rosepigs/-/blob/master/medpseq_nfcore_chipseq.sh)  

Results at : 
>  /work2/genphyse/genepi/data/porc/medpseq/ROSEpigs_processed2/results/

# Analysis
## RnaSeq
differential analysis of rna-seq data using Voom function (Limma package) [Differential Expression with Limma-Voom](https://ucdavis-bioinformatics-training.github.io/2018-June-RNA-Seq-Workshop/thursday/DE.html)
>   linear model :  expr ~ line + condition + sex 

interaction line:condition with the linear model : 
>   expr ~ line + condition + line:condition


## MeDPSeq
## Integrative



